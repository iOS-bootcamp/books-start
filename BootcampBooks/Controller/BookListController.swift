//
//  ViewController.swift
//  BootcampBooks
//
//  Created by Keith Coughtrey on 26/04/17.
//  Copyright © 2017 NativeMobile. All rights reserved.
//

import UIKit
import SVProgressHUD

class BookListController: UIViewController {

    var bookList: BookList?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bookList = BookRepositoryImpl.shared.list(query: "fishing")
    }

}

