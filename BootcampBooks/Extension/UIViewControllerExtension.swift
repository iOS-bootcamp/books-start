//
//  UIViewControllerExtension.swift
//
//  Created by Keith Coughtrey on 22/02/17.
//  Copyright © 2017 Native Mobile. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func showSimpleAlert(title: String, message: String, buttonPressed: (() -> Swift.Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            if let buttonPressed = buttonPressed {
                buttonPressed()
            }
        }))
        present(alert, animated: true, completion: nil)
    }

}
